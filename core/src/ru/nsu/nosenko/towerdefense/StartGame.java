package ru.nsu.nosenko.towerdefense;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import ru.nsu.nosenko.towerdefense.view.MainGameScreen;
import ru.nsu.nosenko.towerdefense.view.MainMenuScreen;

public class StartGame extends Game {
    private SpriteBatch batch;
    private MainMenuScreen mainMenuScreen;
    private MainGameScreen mainGameScreen;

    private StartGame(){}
    private static StartGame instance = new StartGame();
    public static StartGame getInstance(){
        return instance;
    }
    
    @Override
    public void create() {
        batch = new SpriteBatch();
        mainMenuScreen = new MainMenuScreen(batch, this);
        showMenu();
    }
    
    public void showMenu(){
        setScreen(mainMenuScreen);
    }

    public void showMainGameScreen(){
        setScreen(mainGameScreen);
    }

    public boolean isMainGameExist(){
        return (mainGameScreen != null);
    }

    public void newMainGameScreen(int startMoney){
        if (mainGameScreen != null)
            mainGameScreen.dispose();
        mainGameScreen = new MainGameScreen(batch, this, startMoney);
    }

}
