package ru.nsu.nosenko.towerdefense.model.listeners;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import ru.nsu.nosenko.towerdefense.StartGame;

public class BackChangeListener extends ChangeListener {
    StartGame startGame;
    public BackChangeListener(StartGame startGame){
        this.startGame = startGame;
    }
    @Override
    public void changed(ChangeEvent event, Actor actor) {
        startGame.showMenu();
    }
}
