package ru.nsu.nosenko.towerdefense.model.listeners;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import ru.nsu.nosenko.towerdefense.model.menu.UpdateMenu;


public class TurretListener extends InputListener {
    UpdateMenu updateMenu;

    public TurretListener(UpdateMenu updateMenu){
        this.updateMenu = updateMenu;
    }

    @Override
    public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
        updateMenu.update();
        updateMenu.show();
        return false;
    }
}
