package ru.nsu.nosenko.towerdefense.model.listeners;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import ru.nsu.nosenko.towerdefense.model.menu.UpdateMenu;

public class UpdateListener extends ChangeListener {
    UpdateMenu updateMenu;
    public UpdateListener(UpdateMenu updateMenu){
        this.updateMenu = updateMenu;
    }

    @Override
    public void changed(ChangeEvent event, Actor actor) {
        if (updateMenu.getTurret().getNextLvlCost() != -1)
        if (updateMenu.updateCost() != -1 && updateMenu.updateCost() <= updateMenu.gamerMoney()){
            updateMenu.reduceMoney(updateMenu.updateCost());
            updateMenu.getTurret().levelUP();
            updateMenu.update();
        }
    }
}
