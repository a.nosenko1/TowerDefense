package ru.nsu.nosenko.towerdefense.model.listeners;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;

public class ExitChangeListener extends ChangeListener{

    public ExitChangeListener(){
    }
    @Override
    public void changed(ChangeListener.ChangeEvent event, Actor actor) {
        Gdx.app.exit();
    }
}
