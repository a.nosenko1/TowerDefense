package ru.nsu.nosenko.towerdefense.model.listeners;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import ru.nsu.nosenko.towerdefense.model.cells.Cell;
import ru.nsu.nosenko.towerdefense.model.menu.TurretsMenu;

public class TurretCellListener extends InputListener {
    TurretsMenu turretsMenu;
    Cell cell;
    public TurretCellListener(TurretsMenu turretsMenu, Cell cell){
        this.turretsMenu = turretsMenu;
        this.cell = cell;
    }
    @Override
    public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
        turretsMenu.setPositionOverCellAndShow(cell);
        turretsMenu.toFront();
        return false;
    }
}
