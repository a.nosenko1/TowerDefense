package ru.nsu.nosenko.towerdefense.model.listeners;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import ru.nsu.nosenko.towerdefense.model.cells.TurretCell;
import ru.nsu.nosenko.towerdefense.model.menu.TurretsMenu;
import ru.nsu.nosenko.towerdefense.model.menu.UpdateMenu;
import ru.nsu.nosenko.towerdefense.model.turrets.*;

public class TurretMenuListener extends ChangeListener {
    TurretsMenu turretsMenu;  // чтобы узнать деньги
    Turret turret;
    public TurretMenuListener(TurretsMenu turretsMenu, Turret turret){
        super();
        this.turretsMenu = turretsMenu;
        this.turret = turret;
    }
    @Override
    public void changed(ChangeEvent event, Actor actor) {
        if (turret.getLevelCost(1) <= turretsMenu.getGamerMoney()){
            turretsMenu.reduceGamerMoney(turret.getLevelCost(1));
            if (actor.getName().equals("ice")){
                IceTurret iceTurret = new IceTurret((TurretCell)turretsMenu.getCell(), turretsMenu.getMap());
                turretsMenu.getMap().addActor(iceTurret);
                UpdateMenu updateMenu = new UpdateMenu(iceTurret, turretsMenu.getMap(), turretsMenu.getGamerMoney(), turretsMenu.labelMoney);
                iceTurret.addListener(new TurretListener(updateMenu));
                updateMenu.addListener(new UpdateListener(updateMenu));
            }
            if (actor.getName().equals("green")){
                GreenTurret iceTurret = new GreenTurret((TurretCell)turretsMenu.getCell(), turretsMenu.getMap());
                turretsMenu.getMap().addActor(iceTurret);
                UpdateMenu updateMenu = new UpdateMenu(iceTurret, turretsMenu.getMap(), turretsMenu.getGamerMoney(), turretsMenu.labelMoney);
                iceTurret.addListener(new TurretListener(updateMenu));
                updateMenu.addListener(new UpdateListener(updateMenu));
            }
            if (actor.getName().equals("water")){
                WaterTurret iceTurret = new WaterTurret((TurretCell)turretsMenu.getCell(), turretsMenu.getMap());
                turretsMenu.getMap().addActor(iceTurret);
                UpdateMenu updateMenu = new UpdateMenu(iceTurret, turretsMenu.getMap(), turretsMenu.getGamerMoney(), turretsMenu.labelMoney);
                iceTurret.addListener(new TurretListener(updateMenu));
                updateMenu.addListener(new UpdateListener(updateMenu));
            }
            if (actor.getName().equals("fire")){
                FireTurret iceTurret = new FireTurret((TurretCell)turretsMenu.getCell(), turretsMenu.getMap());
                turretsMenu.getMap().addActor(iceTurret);
                UpdateMenu updateMenu = new UpdateMenu(iceTurret, turretsMenu.getMap(), turretsMenu.getGamerMoney(), turretsMenu.labelMoney);
                iceTurret.addListener(new TurretListener(updateMenu));
                updateMenu.addListener(new UpdateListener(updateMenu));
            }
            turretsMenu.hide();
           // newTurret.addListener()
        }
    }
}
