package ru.nsu.nosenko.towerdefense.model.map;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import ru.nsu.nosenko.towerdefense.model.cells.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Vector;

public class Map extends Group {
    public static int getCellSize(){return 100;}

    private int cellsX;
    private int cellsY;
    public Cell[][] field;
    private LinkedList<RoadCell> path;
    public Cell[] pathArray;

    public int getCellsX(){
        return cellsX;
    }
    public int getCellsY(){
        return cellsY;
    }

    public Map(int cellsX, int cellsY, Cell[][] field, LinkedList<RoadCell> path, Cell[] pathArray){
        mapActors = new HashSet();
        this.cellsX = cellsX;
        this.cellsY = cellsY;
        setSize(cellsX*getCellSize(),cellsY*getCellSize());
        this.field = field;
        this.path = path;
        this.pathArray = pathArray;
        for (int i = 0; i < cellsY; i++) {
            for (int j = 0; j < cellsX; j++) {
                if (field[j][i] != null)
                    addActor(field[j][i]);
            }
        }
        for (int i = 0; i < cellsY; i++) {
            for (int j = 0; j < cellsX; j++) {
                if (field[j][i] != null)
                    field[j][i].setPosition(j*getCellSize(),i*getCellSize());
            }
        }
    }

    public void setCell(Cell cell, int x, int y){
        removeActor(field[x][y]);
        field[x][y] = cell;
        field[x][y].setPosition(x*getCellSize(),y*getCellSize());
        addActor(field[x][y]);
        // listener???
    }

    public HashSet<Actor> mapActors;
    @Override
    public void addActor(Actor actor) {
        super.addActor(actor);
        mapActors.add(actor); // опять костыль!
    }

    public static class MapFactory {

        /* Creates an empty map */
        public static Map newEmptyMap(int cellsX, int cellsY){
            Cell[][] field = new Cell[cellsX][cellsY];
            return new Map(cellsX, cellsY, field, null, null);
        }
        /* Creates a simple map 16x12 */
        public static Map newSimpleMap(){
            int cellsX = 15;
            int cellsY = 8;
            Cell[][] field = new Cell[cellsX][cellsY];

            Cell[] pathArray = new Cell[25];

            field[14][1] = new MobSpawnerCell(14, 1);
            field[14][1].rotate(90);
            field[2][7] = new BaseCell(2, 7);

            LinkedList<RoadCell> path = new LinkedList<>();
            for (int x = 12; x <= 13; x++) {
                path.add((RoadCell)(field[x][1] = new RoadCell(x, 1)));
            }

            path.add((RoadCell)(field[11][1] = new RoadAngleCell(11, 1)));
            field[11][1].rotate(180);
            for (int y = 2; y <= 4; y++) {
                path.add((RoadCell)(field[11][y] = new RoadCell(11, y)));
                field[11][y].rotate(90);
            }
            path.add((RoadCell)(field[11][5] = new RoadAngleCell(11, 5)));

            for (int x = 7; x <= 10; x++) {
                path.add((RoadCell)(field[x][5] = new RoadCell(x, 5)));
            }
            path.add((RoadCell)(field[6][5] = new RoadAngleCell(6, 5)));
            field[6][5].rotate(90);

            for (int y = 3; y <= 4; y++) {
                path.add((RoadCell)(field[6][y] = new RoadCell(6, y)));
                field[6][y].rotate(90);
            }
            path.add((RoadCell)(field[6][2] = new RoadAngleCell(6, 2)));
            field[6][2].rotate(-90);
            for (int x = 3; x <= 5; x++) {
                path.add((RoadCell)(field[x][2] = new RoadCell(x, 2)));
            }
            path.add((RoadCell)(field[2][2] = new RoadAngleCell(2, 2)));
            field[2][2].rotate(180);

            for (int y = 3; y <= 6; y++) {
                path.add((RoadCell)(field[2][y] = new RoadCell(2, y)));
                field[2][y].rotate(90);
            }
            assert (path.size() == 23);
            // костыль!!
            pathArray[0] = field[14][1];
            pathArray[25-1] = field[2][7];
            pathArray[1] = field[13][1];
            pathArray[2] = field[12][1];
            pathArray[3] = field[11][1];
            pathArray[4] = field[11][2];
            pathArray[5] = field[11][3];
            pathArray[6] = field[11][4];
            pathArray[7] = field[11][5];
            pathArray[8] = field[10][5];
            pathArray[9] = field[9][5];
            pathArray[10] = field[8][5];
            pathArray[11] = field[7][5];
            pathArray[12] = field[6][5];
            pathArray[13] = field[6][4];
            pathArray[14] = field[6][3];
            pathArray[15] = field[6][2];
            pathArray[16] = field[5][2];
            pathArray[17] = field[4][2];
            pathArray[18] = field[3][2];
            pathArray[19] = field[2][2];
            pathArray[20] = field[2][3];
            pathArray[21] = field[2][4];
            pathArray[22] = field[2][5];
            pathArray[23] = field[2][6];

            field[1][5] = new TurretCell(1,5);
            field[3][5] = new TurretCell(3,5);
            field[4][1] = new TurretCell(4,1);
            field[7][4] = new TurretCell(7,4);
            field[7][6] = new TurretCell(7,6);
            field[10][4] = new TurretCell(10,4);
            field[11][6] = new TurretCell(11,6);
            field[12][5] = new TurretCell(12,5);
            field[10][1] = new TurretCell(10,1);
            field[11][0] = new TurretCell(11,0);
            field[12][2] = new TurretCell(12,2);
            field[3][3] = new TurretCell(3,3);
            field[5][3] = new TurretCell(5,3);



            return new Map(cellsX, cellsY, field, null, pathArray);
        }

        public static Cell[] createPathArray(
                LinkedList<RoadCell> path,
                MobSpawnerCell mobSpawnerCell,
                BaseCell baseCell,
                int pathSize){
            Vector2[] pathArray = new Vector2[pathSize];
            pathArray[0] = new Vector2(mobSpawnerCell.fieldX, mobSpawnerCell.fieldY);
            pathArray[pathSize-1] = new Vector2(baseCell.fieldX, baseCell.fieldY);
        return null;
            // сложно, потом сделаю
        }

    }


}


