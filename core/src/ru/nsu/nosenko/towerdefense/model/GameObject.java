package ru.nsu.nosenko.towerdefense.model;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class GameObject extends Actor {
    public Sprite sprite;

    @Override
    public void draw(Batch batch, float parentAlpha) {
        /* зависит только от полигона */
        sprite.setPosition(polygon.getX(), polygon.getY());
        sprite.setRotation(polygon.getRotation());
        sprite.draw(batch);
    }

    Polygon polygon;

    public GameObject(Texture texture, float x, float y, float width, float height){

        sprite = new Sprite(texture);
        sprite.setSize(width, height);
        sprite.setOrigin(width/2, height/2); // центр поворота
        sprite.setPosition(x, y);

        polygon = new Polygon(new float[]{0f, 0f, width, 0f, width, height, 0f, height});
        polygon.setPosition(x, y);
        polygon.setOrigin(width/2, height/2); // центр поворота

        setBounds(getPolygon().getX(), getPolygon().getY(),
                getPolygon().getBoundingRectangle().getWidth(),
                getPolygon().getBoundingRectangle().getHeight());

    }

    public void setPosition(float x, float y){
        super.setPosition(x, y);
        sprite.setPosition(x, y);
        polygon.setPosition(x, y);
        setBounds(getPolygon().getX(), getPolygon().getY(),
                getPolygon().getBoundingRectangle().getWidth(),
                getPolygon().getBoundingRectangle().getHeight());

    }

    public void rotate(float deg){
        polygon.setRotation(deg);
    }

    public Polygon getPolygon(){
        return polygon;
    }

    public void changeTexture(Texture newTexture){
        float width = sprite.getWidth();
        float height = sprite.getHeight();
        float x = sprite.getX();
        float y = sprite.getY();

        sprite = new Sprite(newTexture);

        sprite.setSize(width, height);
        sprite.setOrigin(width/2, height/2); // центр поворота
        sprite.setPosition(x, y);

        polygon = new Polygon(new float[]{0f, 0f, width, 0f, width, height, 0f, height});
        polygon.setPosition(x, y);
        polygon.setOrigin(width/2, height/2); // центр поворота

        setBounds(getPolygon().getX(), getPolygon().getY(),
                getPolygon().getBoundingRectangle().getWidth(),
                getPolygon().getBoundingRectangle().getHeight());
    }
}
