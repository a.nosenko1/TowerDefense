package ru.nsu.nosenko.towerdefense.model.menu;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import ru.nsu.nosenko.towerdefense.model.buttons.MyButton;
import ru.nsu.nosenko.towerdefense.model.map.Map;
import ru.nsu.nosenko.towerdefense.model.turrets.Turret;

public class UpdateMenu extends Group {
    Texture nextLevelTexture;
    Turret turret;
    Map map;
    Skin skin = new Skin(Gdx.files.internal("boldLabel.json"));
    Label label;
    MyButton updateButton;
    Integer gamerMoney;
    Label labelMoney;

    public UpdateMenu(Turret turret, Map map, Integer gamerMoney, Label labelMoney){
        this.gamerMoney = gamerMoney;
        this.labelMoney = labelMoney;
        this.turret = turret;
        this.map = map;
        update();
    }

    public void update(){
        if (updateButton != null)
            map.removeActor(updateButton);
        if (label != null)
            map.removeActor(label);
        if (turret.getLevelTexture(turret.getLvl()+1) != null){
            nextLevelTexture = turret.getLevelTexture(turret.getLvl()+1);
            updateButton = new MyButton(nextLevelTexture);
            int cost = turret.getNextLvlCost();
            label = new Label(cost + "$", skin);

            label.setPosition(turret.turretCell.getX()+50, turret.turretCell.getY()+90);
            updateButton.setPosition(turret.turretCell.getX()+50, turret.turretCell.getY()+80);

            label.setFontScale(0.5f);
            label.setSize(1,1);

            addActor(updateButton);
            addActor(label);

            map.addActor(this);

            hide();
        }
    }

    public void show(){
        if (turret.getNextLvlCost() != -1){
            if (label != null)
                label.setVisible(true);
            if (updateButton != null)
                updateButton.setVisible(true);
        }
    }
    public void hide(){
        if (label != null)
            label.setVisible(false);
        if (updateButton != null)
            updateButton.setVisible(false);
    }
    public int updateCost(){
        return turret.getNextLvlCost();
    }
    public Integer gamerMoney(){
        return gamerMoney;
    }
    public void reduceMoney(int minus){
        gamerMoney -= minus;
        labelMoney.setText("$ " + gamerMoney.toString());
        hide();
    }
    public Turret getTurret(){
        return turret;
    }

}
