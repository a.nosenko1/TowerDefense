package ru.nsu.nosenko.towerdefense.model.menu;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import ru.nsu.nosenko.towerdefense.model.GameObject;
import ru.nsu.nosenko.towerdefense.model.buttons.MyButton;
import ru.nsu.nosenko.towerdefense.model.cells.Cell;
import ru.nsu.nosenko.towerdefense.model.cells.TurretCell;
import ru.nsu.nosenko.towerdefense.model.map.Map;
import ru.nsu.nosenko.towerdefense.model.turrets.FireTurret;
import ru.nsu.nosenko.towerdefense.model.turrets.GreenTurret;
import ru.nsu.nosenko.towerdefense.model.turrets.IceTurret;
import ru.nsu.nosenko.towerdefense.model.turrets.WaterTurret;


public class TurretsMenu extends Group {

    private Cell curCell;
    public Cell getCell(){
        return curCell;
    }
    private Map map;
    public Map getMap(){
        return map;
    }
    Integer gamerMoney;
    public Label labelMoney;

    public MyButton blue1;
    public MyButton white1;
    public MyButton red1;
    public MyButton green1;

    FireTurret fireTurret = new FireTurret(new TurretCell(-1, -1), map); // чтобы узнать цену
    WaterTurret waterTurret = new WaterTurret(new TurretCell(-1, -1), map); // чтобы узнать цену
    GreenTurret greenTurret = new GreenTurret(new TurretCell(-1, -1), map); // чтобы узнать цену
    IceTurret iceTurret = new IceTurret(new TurretCell(-1, -1), map); // чтобы узнать цену

    public TurretsMenu(Integer gamerMoney, Label labelMoney, Map map){
        this.map = map;
        this.gamerMoney = gamerMoney;
        this.labelMoney = labelMoney;
        GameObject frame = new GameObject(new Texture(Gdx.files.internal("Turrets/frame.png")),
                46, 46, 160, 160);
        addActor(frame);

        Integer cost;
        Skin skin = new Skin(Gdx.files.internal("boldLabel.json"));

        blue1 = new MyButton(new Texture(Gdx.files.internal("Turrets/blue1.png")));
        blue1.setPosition(0, 0);
        blue1.setName("water");
        addActor(blue1);
        cost = waterTurret.getLevelCost(1);
        Label blue1Label = new Label(cost.toString()+"$", skin);
        blue1Label.setPosition(blue1.getX()-5, blue1.getY()+15);
        addActor(blue1Label);
        blue1Label.setFontScale(0.5f);
        blue1Label.setSize(1,1);
        blue1Label.setColor(0, 0.627f ,1f, 1);

        red1 = new MyButton(new Texture(Gdx.files.internal("Turrets/red1.png")));
        red1.setPosition(0, 153);
        red1.setName("fire");
        addActor(red1);
        cost = fireTurret.getLevelCost(1);
        Label red1Label = new Label(cost.toString()+"$", skin);
        red1Label.setPosition(red1.getX()-5, red1.getY()+15);
        addActor(red1Label);
        red1Label.setFontScale(0.5f);
        red1Label.setSize(1,1);
        red1Label.setColor(0.87f, 0.509f ,0.56f, 1);

        green1 = new MyButton(new Texture(Gdx.files.internal("Turrets/green1.png")));
        green1.setPosition(153, 0);
        green1.setName("green");
        addActor(green1);
        cost = greenTurret.getLevelCost(1);
        Label green1Label = new Label(cost.toString()+"$", skin);
        green1Label.setPosition(green1.getX()-5, green1.getY()+15);
        addActor(green1Label);
        green1Label.setFontScale(0.5f);
        green1Label.setSize(1,1);
        green1Label.setColor(0.45f, 1f ,0.43f, 1);

        white1 = new MyButton(new Texture(Gdx.files.internal("Turrets/white1.png")));
        white1.setPosition(153, 153);
        white1.setName("ice");
        addActor(white1);
        cost = iceTurret.getLevelCost(1);
        Label white1Label = new Label(cost.toString()+"$", skin);
        white1Label.setPosition(white1.getX()-5, white1.getY()+15);
        addActor(white1Label);
        white1Label.setFontScale(0.5f);
        white1Label.setSize(1,1);
        white1Label.setColor(1f, 1f ,1f, 1);
    }

    public void setPositionOverCell(Cell cell){
        this.setPosition(cell.getX()-76, cell.getY()-76);
        curCell = cell;
    }

    public void hide(){
        this.setVisible(false);
        curCell = null;
    }
    public void show(){
        this.setVisible(true);
    }
    public void setPositionOverCellAndShow(Cell cell){
        setPositionOverCell(cell);
        show();
    }
    public Integer getGamerMoney(){
        return gamerMoney;
    }
    public void reduceGamerMoney(int minus){
        gamerMoney -= minus;
        labelMoney.setText("$ " + getGamerMoney().toString());
    }
    public boolean isShow(){
        return this.isVisible();
    }
    public boolean isThisPosition(Cell cell){
        return (cell == curCell);
    }


}
