package ru.nsu.nosenko.towerdefense.model.turrets;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import ru.nsu.nosenko.towerdefense.model.bullets.Bullet;
import ru.nsu.nosenko.towerdefense.model.cells.TurretCell;
import ru.nsu.nosenko.towerdefense.model.map.Map;

public class WaterTurret extends Turret{
    private static final int MAX_LVL = 3;

    public WaterTurret(TurretCell turretCell, Map map) {
        super(turretCell, new Texture(Gdx.files.internal("Turrets/blue1.png")), MAX_LVL, map);
    }
    @Override
    public Bullet.BulletType getBulletType() {
        return Bullet.BulletType.Water;
    }

    @Override
    public int getMaxLevel() {
        return MAX_LVL;
    }



    public int getLevelCost(int level) {
        switch (level){
            case 1:
                return 50;
            case 2:
                return 100;
            case 3:
                return 150;
        }
        return -1;
    }

    @Override
    public Texture getLevelTexture(int level) {
        switch (level){
            case 1:
                return new Texture(Gdx.files.internal("Turrets/blue1.png"));
            case 2:
                return new Texture(Gdx.files.internal("Turrets/blue2.png"));
            case 3:
                return new Texture(Gdx.files.internal("Turrets/blue3.png"));
        }
        return null;
    }
}
