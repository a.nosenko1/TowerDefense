package ru.nsu.nosenko.towerdefense.model.turrets;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import ru.nsu.nosenko.towerdefense.model.bullets.Bullet;
import ru.nsu.nosenko.towerdefense.model.cells.TurretCell;
import ru.nsu.nosenko.towerdefense.model.map.Map;

public class FireTurret extends Turret{
    private static int MAX_LEVEL = 3;
    Map map;

    public FireTurret(TurretCell turretCell, Map map) {
        super(turretCell, new Texture(Gdx.files.internal("Turrets/red1.png")), MAX_LEVEL, map);
        this.map = map;
    }

    @Override
    public Bullet.BulletType getBulletType() {
        return Bullet.BulletType.Fire;
    }

    @Override
    public int getMaxLevel() {
        return MAX_LEVEL;
    }

    public int getLevelCost(int level) {
        switch (level){
            case 1:
                return 100;
            case 2:
                return 150;
            case 3:
                return 250;
        }
        return -1;
    }

    @Override
    public Texture getLevelTexture(int level) {
        switch (level){
            case 1:
                return new Texture(Gdx.files.internal("Turrets/red1.png"));
            case 2:
                return new Texture(Gdx.files.internal("Turrets/red2.png"));
            case 3:
                return new Texture(Gdx.files.internal("Turrets/red3.png"));
        }
        return null;
    }


}
