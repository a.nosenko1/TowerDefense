package ru.nsu.nosenko.towerdefense.model.turrets;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import ru.nsu.nosenko.towerdefense.model.bullets.Bullet;
import ru.nsu.nosenko.towerdefense.model.cells.TurretCell;
import ru.nsu.nosenko.towerdefense.model.map.Map;

public class IceTurret extends Turret{
    private static final int MAX_LVL = 3;

    public IceTurret(TurretCell turretCell, Map map) {
        super(turretCell, new Texture(Gdx.files.internal("Turrets/white1.png")), MAX_LVL, map);
    }

    @Override
    public Bullet.BulletType getBulletType() {
        return Bullet.BulletType.Snow;
    }

    @Override
    public int getMaxLevel() {
        return MAX_LVL;
    }

    public int getLevelCost(int level) {
        switch (level){
            case 1:
                return 100;
            case 2:
                return 150;
            case 3:
                return 250;
        }
        return -1;
    }

    @Override
    public Texture getLevelTexture(int level) {
        switch (level){
            case 1:
                return new Texture(Gdx.files.internal("Turrets/white1.png"));
            case 2:
                return new Texture(Gdx.files.internal("Turrets/white2.png"));
            case 3:
                return new Texture(Gdx.files.internal("Turrets/white3.png"));
        }
        return null;
    }
}
