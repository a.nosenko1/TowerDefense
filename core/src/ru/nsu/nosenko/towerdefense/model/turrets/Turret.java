package ru.nsu.nosenko.towerdefense.model.turrets;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import ru.nsu.nosenko.towerdefense.model.GameObject;
import ru.nsu.nosenko.towerdefense.model.bullets.Bullet;
import ru.nsu.nosenko.towerdefense.model.cells.TurretCell;
import ru.nsu.nosenko.towerdefense.model.map.Map;
import ru.nsu.nosenko.towerdefense.model.mobs.Mob;

import java.util.ConcurrentModificationException;
import java.util.HashMap;
import java.util.HashSet;

public abstract class Turret extends GameObject {

    private static final int RADIUS = 150;

    public TurretCell turretCell;
    public abstract Bullet.BulletType getBulletType();

    public abstract int getMaxLevel();
    public abstract int getLevelCost(int level);
    public abstract Texture getLevelTexture(int level);

    private int curLevel;
    private Map map;

    private float myX;
    private float myY;

    private HashMap<Integer, Integer> levelToCost;
    private HashMap<Integer, Texture> levelToTexture;

    public Turret(TurretCell turretCell, Texture texture, int maxLevel, Map map) {
        super(texture, turretCell.getX(), turretCell.getY(), turretCell.getWidth(), turretCell.getHeight());
        this.turretCell = turretCell;
        this.map = map;
        this.curLevel = 1;
        myX = turretCell.getX()+50;
        myY = turretCell.getY()+50;

        levelToCost = new HashMap<>(maxLevel);
        for (int i = 0; i < maxLevel; i++) {
            levelToCost.put(i+1,getLevelCost(i+1));
        }
        levelToTexture = new HashMap<>(maxLevel);
        for (int i = 0; i < maxLevel; i++) {
            levelToTexture.put(i+1,getLevelTexture(i+1));
        }
    }
    float time = 0;
    Mob mob = null;
    @Override
    public void act(float delta) {
        super.act(delta);
        time += delta;
        mob = null;
        try{
            for (Actor actor: map.mapActors) {
                if (actor.isVisible()){
                    if (actor == this) continue;
                    if ((actor.getX()+25-myX)*(actor.getX()+25-myX) + (actor.getY()+25-myY)*(actor.getY()+25-myY) < RADIUS*RADIUS ){
                        if (actor.isVisible())
                            try {
                                mob = (Mob)actor;
                            } catch (Exception ignored) {
                            }
                    }
                }
            }
        }catch (ConcurrentModificationException ignored){
        }

        if (time > 0.7){
            time = 0;
            if (mob != null){
                if ((mob.getX()+25-myX)*(mob.getX()+25-myX) + (mob.getY()+25-myY)*(mob.getY()+25-myY) < RADIUS*RADIUS ){
                    (mob).hit(new Bullet(new Texture(Gdx.files.internal("bullet.png")), myX, myY, getBulletType(), curLevel * curLevel));

                    ShapeRenderer shapeRenderer = new ShapeRenderer();
                    drawDottedLine(shapeRenderer, 1, (myX + 116) * 0.59f, (myY + 248) * 0.586f, (mob.getX() + 25 + 116) * 0.59f, (mob.getY() + 25 + 248) * 0.586f);
                    mob = null;
                }
                 }

        }

    }


    private void drawDottedLine(ShapeRenderer shapeRenderer, int dotDist, float x1, float y1, float x2, float y2) {
        shapeRenderer.begin(ShapeRenderer.ShapeType.Point);

        Vector2 vec2 = new Vector2(x2, y2).sub(new Vector2(x1, y1));
        float length = vec2.len();
        for(int i = 0; i < length; i += dotDist) {
            vec2.clamp(length - i, length - i);
            shapeRenderer.point(x1 + vec2.x, y1 + vec2.y, 0);
        }

        shapeRenderer.end();
    }

    public int getNextLvlCost(){
        if (curLevel != getMaxLevel()){
            return levelToCost.get(curLevel + 1);
        }
        return -1;
    }

    public int getLvl(){
        return curLevel;
    }

    public void levelUP(){
        if (curLevel != getMaxLevel()){
            curLevel++;
            this.changeTexture(getLevelTexture(curLevel));
        }
    }

}


