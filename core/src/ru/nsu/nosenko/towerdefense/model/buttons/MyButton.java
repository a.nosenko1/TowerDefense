package ru.nsu.nosenko.towerdefense.model.buttons;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import ru.nsu.nosenko.towerdefense.model.tools.UsefulTool;

public class MyButton extends ImageButton {
    public MyButton(Texture texture) {

        super(new SpriteDrawable(new Sprite(texture)),
                (new SpriteDrawable(UsefulTool.reduce(new Sprite(texture),0.9f, 0.9f))));

    }
}
