package ru.nsu.nosenko.towerdefense.model.mobs;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import ru.nsu.nosenko.towerdefense.model.bullets.Bullet;
import ru.nsu.nosenko.towerdefense.model.GameObject;
import ru.nsu.nosenko.towerdefense.model.map.Map;
import ru.nsu.nosenko.towerdefense.view.MainGameScreen;


public class Mob extends GameObject {
    private int level;
    private int health;
    private final int SPEED_CONST = 50; // pixels per second
    private int HEALTH_CONST;
    private int curSpeed;
    private Map map;

    private MainGameScreen mainGameScreen;

    private Vector3 velosity;
    private Vector3 position;

    private float watering = 0;
    private float snowing = 0;

    private MobFactory.MobType mobType;

    public int getLevel(){
        return level;
    }
    public int getCost(){
        return 50*level*level;
    }

    public Mob(Texture texture, float x, float y, int level, MobFactory.MobType mobType, Map map,
               MainGameScreen mainGameScreen) {
        super(texture, x, y, 50, 50);
        this.mainGameScreen = mainGameScreen;
        this.level = level;
        this.map = map;
        health = 10*level*level;
        HEALTH_CONST = health;
        this.mobType = mobType;
        curSpeed = SPEED_CONST;
        position = new Vector3(x, y, 0);
        velosity = new Vector3(0, 0, 0);
        wayPoint = new Vector2(x, y);
    }

    public void setCoord(float x, float y){
        this.setPosition(x, y);
    }

    public void setFreeze(boolean isFreeze){
        if (isFreeze) toFreeze();
        else unFreeze();
    }

    private void toFreeze(){
        this.changeTexture(MobFactory.getTextureIceToMobType(mobType));
        this.curSpeed = 0;
    }

    private void unFreeze(){
        this.changeTexture(MobFactory.getTextureToMobType(mobType));
        this.curSpeed = SPEED_CONST;
    }

    public void hit (Bullet bullet){
        System.out.println(health);
        health -= MobFactory.damageCoef(mobType, bullet.getBulletType())*bullet.getDamage();
        update();
        if (bullet.getBulletType() == Bullet.BulletType.Water) watering += 0.2;
        if (bullet.getBulletType() == Bullet.BulletType.Snow) snowing += 0.2; //TODO не забыть отнимать
    }

    private void update(){
        if (health < 0){
            // смерть
            mainGameScreen.addMoney(getCost());
            this.setVisible(false);
//            map.removeActor(this);
//            map.mapActors.remove(this);
        }
        this.sprite.setAlpha(-((float)health/HEALTH_CONST-1)*((float)health/HEALTH_CONST-1)*((float)health/HEALTH_CONST-1)*((float)health/HEALTH_CONST-1)+1);
    }

    Vector2 wayPoint;
    int i = 0;
    @Override
    public void act(float delta) {
        try {
            // дошли, берем некст координаты
            if (Math.abs(this.position.x - wayPoint.x) < 5 && Math.abs(this.position.y - wayPoint.y) < 5 ){
                i++;
                // System.out.println("ДОШЛИ " + i);
                wayPoint = getNextCoord();
                if (wayPoint == null){
                    // дошли до крепости
                    mainGameScreen.reduceHealth(1);
                    this.sprite.setAlpha(0);
                    map.removeActor(this);
                    map.mapActors.remove(this);
                    return;
                }
                // вычисляем, куда идти
                if (this.position.x - wayPoint.x > 5){
                    velosity.x = -SPEED_CONST;
                    velosity.y = 0;
                }
                else if (this.position.x - wayPoint.x < -5){
                    velosity.x = SPEED_CONST;
                    velosity.y = 0;
                }else if (this.position.y - wayPoint.y > 5){
                    velosity.x = 0;
                    velosity.y = -SPEED_CONST;
                }else{
                    velosity.x = 0;
                    velosity.y = SPEED_CONST;
                }
            }

            Vector3 vel = new Vector3(velosity.x, velosity.y, 0);
            vel.scl(delta);
            position.add(vel.x, vel.y, 0);
            this.setPosition(position.x, position.y);
        }catch (NullPointerException ignored){

        }

    }

    private int numCurCell = 0;
    private Vector2 getNextCoord(){
        numCurCell++;
        if (numCurCell != map.pathArray.length)
            return new Vector2(map.pathArray[numCurCell].getX()+25, map.pathArray[numCurCell].getY()+25);
        else return null;
    }
}
