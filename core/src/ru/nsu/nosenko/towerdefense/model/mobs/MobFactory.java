package ru.nsu.nosenko.towerdefense.model.mobs;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import ru.nsu.nosenko.towerdefense.model.bullets.Bullet;
import ru.nsu.nosenko.towerdefense.model.map.Map;
import ru.nsu.nosenko.towerdefense.view.MainGameScreen;

public class MobFactory {

    private static final Texture red1 = new Texture(Gdx.files.internal("mobs/red1.png"));
    private static final Texture red2 = new Texture(Gdx.files.internal("mobs/red2.png"));
    private static final Texture red3 = new Texture(Gdx.files.internal("mobs/red3.png"));
    private static final Texture blue1 = new Texture(Gdx.files.internal("mobs/blue1.png"));
    private static final Texture blue2 = new Texture(Gdx.files.internal("mobs/blue2.png"));
    private static final Texture blue3 = new Texture(Gdx.files.internal("mobs/blue3.png"));
    private static final Texture yellow1 = new Texture(Gdx.files.internal("mobs/yellow1.png"));
    private static final Texture yellow2 = new Texture(Gdx.files.internal("mobs/yellow2.png"));
    private static final Texture yellow3 = new Texture(Gdx.files.internal("mobs/yellow3.png"));
    private static final Texture    red1ice = new Texture(Gdx.files.internal("mobs/red1ice.png"));
    private static final Texture    red2ice = new Texture(Gdx.files.internal("mobs/red2ice.png"));
    private static final Texture    red3ice = new Texture(Gdx.files.internal("mobs/red3ice.png"));
    private static final Texture   blue1ice = new Texture(Gdx.files.internal("mobs/blue1ice.png"));
    private static final Texture   blue2ice = new Texture(Gdx.files.internal("mobs/blue2ice.png"));
    private static final Texture   blue3ice = new Texture(Gdx.files.internal("mobs/blue3ice.png"));
    private static final Texture yellow1ice = new Texture(Gdx.files.internal("mobs/yellow1ice.png"));
    private static final Texture yellow2ice = new Texture(Gdx.files.internal("mobs/yellow2ice.png"));
    private static final Texture yellow3ice = new Texture(Gdx.files.internal("mobs/yellow3ice.png"));

    public enum MobType{RED1, RED2, RED3, BLUE1, BLUE2, BLUE3, YELLOW1, YELLOW2, YELLOW3}
    // level - коэффициент здоровья, минимум 1
    public static Mob newMob(MobType mobType, int level, float x, float y, Map map,
                             MainGameScreen mainGameScreen){
        return new Mob(getTextureToMobType(mobType), x, y, level, mobType, map, mainGameScreen);
    }

    public static Texture getTextureToMobType(MobType mobType){
        switch (mobType){
            case RED1: return red1;
            case RED2: return red2;
            case RED3: return red3;
            case BLUE1: return blue1;
            case BLUE2: return blue2;
            case BLUE3: return blue3;
            case YELLOW1: return yellow1;
            case YELLOW2: return yellow2;
            case YELLOW3: return yellow3;
        }
        return null;
    }
    public static Texture getTextureIceToMobType(MobType mobType){
        switch (mobType){
            case RED1: return          red1ice;
            case RED2: return          red2ice;
            case RED3: return          red3ice;
            case BLUE1: return        blue1ice;
            case BLUE2: return        blue2ice;
            case BLUE3: return        blue3ice;
            case YELLOW1: return    yellow1ice;
            case YELLOW2: return    yellow2ice;
            case YELLOW3: return    yellow3ice;
        }
        return null;
    }
    public static float damageCoef(MobType mobType, Bullet.BulletType bulletType){
        switch (bulletType){
            case Fire: {
                switch (mobType){
                    case RED1:
                    case RED2:
                    case RED3:
                        return 0.5f;
                    case BLUE1:
                    case BLUE2:
                    case BLUE3:
                        return 1.5f;
                    case YELLOW1:
                    case YELLOW2:
                    case YELLOW3:
                        return 1;
                }
            }
            case Water: {
                switch (mobType){
                    case RED1:
                    case RED2:
                    case RED3:
                        return 1.5f;
                    case BLUE1:
                    case BLUE2:
                    case BLUE3:
                        return 0.5f;
                    case YELLOW1:
                    case YELLOW2:
                    case YELLOW3:
                        return 1;
                }
            }
            case Snow: {
                switch (mobType){
                    case RED1:
                    case RED2:
                    case RED3:
                        return 1.2f;
                    case BLUE1:
                    case BLUE2:
                    case BLUE3:
                        return 0.5f;
                    case YELLOW1:
                    case YELLOW2:
                    case YELLOW3:
                        return 0.8f;
                }
            }
            case Green: {
                switch (mobType){
                    case RED1:
                    case RED2:
                    case RED3:
                        return 1f;
                    case BLUE1:
                    case BLUE2:
                    case BLUE3:
                        return 0.8f;
                    case YELLOW1:
                    case YELLOW2:
                    case YELLOW3:
                        return 1.2f;
                }
            }
        }
        return 0;
    }
}
