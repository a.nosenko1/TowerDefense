package ru.nsu.nosenko.towerdefense.model.cells;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;

public class RoadAngleCell extends RoadCell{
    public RoadAngleCell(int x, int y) {
        super(x, y, new Texture(Gdx.files.internal("Cells/RoadAngleCell.png")));
    }
}
