package ru.nsu.nosenko.towerdefense.model.cells;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import ru.nsu.nosenko.towerdefense.model.map.Map;

public class RoadCell extends Cell {

    public RoadCell(int x, int y){
        super(new Texture(Gdx.files.internal("Cells/RoadCell.png")),
                0, 0, Map.getCellSize(), Map.getCellSize(), x, y);
    }

    public RoadCell(int x, int y, Texture texture){
        super(texture,
                0, 0, Map.getCellSize(), Map.getCellSize(), x, y);
    }

}
