package ru.nsu.nosenko.towerdefense.model.cells;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Stage;
import ru.nsu.nosenko.towerdefense.model.GameObject;

public abstract class Cell extends GameObject {
    public int fieldX;
    public int fieldY;

    public Cell(Texture texture, float x, float y, float width, float height, int fieldX, int fieldY) {
        super(texture, x, y, width, height);
        this.fieldX = fieldX;
        this.fieldY = fieldY;
    }


}



