package ru.nsu.nosenko.towerdefense.model.cells;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import ru.nsu.nosenko.towerdefense.model.map.Map;

public class BaseCell extends Cell {
    public BaseCell(int x, int y){
        super(new Texture(Gdx.files.internal("Cells/BaseCell.png")),
                0, 0, Map.getCellSize(), Map.getCellSize(), x, y);
    }
}
