package ru.nsu.nosenko.towerdefense.model.cells;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import ru.nsu.nosenko.towerdefense.model.map.Map;
import ru.nsu.nosenko.towerdefense.model.turrets.Turret;

public class TurretCell extends Cell {
    private Turret turret = null;

    public TurretCell(int x, int y){
        super(new Texture(Gdx.files.internal("Cells/TurretCell.png")),
                0, 0, Map.getCellSize(), Map.getCellSize(), x, y);
        this.setVisible(false); // ухх костыль для того, чтобы при отображении найти все туррельные ячейки
    }

    public void setTurret(Turret turret){
        this.turret = turret;
    }

    public Turret getTurret(){
        return this.turret;
    }

    public void removeTurret(){
        this.turret = null;
    }
}
