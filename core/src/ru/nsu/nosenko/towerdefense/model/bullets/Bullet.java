package ru.nsu.nosenko.towerdefense.model.bullets;

import com.badlogic.gdx.graphics.Texture;
import ru.nsu.nosenko.towerdefense.model.GameObject;

public class Bullet extends GameObject {
    public enum BulletType {Fire, Water, Snow, Green}
    private final int SPEED = 100; // pixels per second
    private BulletType bulletType;
    private int damage;
    public Bullet(Texture texture, float x, float y, BulletType bulletType, int damage) {
        super(texture, x, y, 10, 10);
        this.bulletType = bulletType;
        this.damage = damage;
    }

    public int getDamage(){
        return damage;
    }
    public BulletType getBulletType(){
        return bulletType;
    }

    @Override
    public void act(float delta) {
        super.act(delta);

    }
}
