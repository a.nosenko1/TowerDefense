package ru.nsu.nosenko.towerdefense.model.tools;

import ru.nsu.nosenko.towerdefense.view.MainGameScreen;

import java.net.*;
import java.io.*;

class GameClient {
    private final MainGameScreen mainGameScreen;
    private Socket socket;
    private BufferedReader in; // поток чтения из сокета
    private BufferedWriter out; // поток чтения в сокет
    private String addr; // ip адрес клиента
    private int port; // порт соединения

    public GameClient(MainGameScreen mainGameScreen, String addr, int port) {
        this.mainGameScreen = mainGameScreen;
        this.addr = addr;
        this.port = port;
        try {
            this.socket = new Socket(addr, port);
        } catch (IOException|NullPointerException e) {
            System.err.println("Socket failed");
        }
        try {
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));

            new ReadMsg().start(); // нить читающая сообщения из сокета в бесконечном цикле
            new WriteMsg().start(); // нить пишущая сообщения в сокет приходящие с консоли в бесконечном цикле
        } catch (IOException e) {
            GameClient.this.downService();
        }

    }

    private void downService() {
        try {
            if (!socket.isClosed()) {
                socket.close();
                in.close();
                out.close();
            }
        } catch (IOException ignored) {}
    }

    // нить чтения сообщений с сервера
    private class ReadMsg extends Thread {
        @Override
        public void run() {
            String str;
            try {
                while (true) {
                    str = in.readLine(); // ждем сообщения с сервера
                    if (str.equals("YouLose")) {
                        mainGameScreen.hide();
                    }
                }
            } catch (IOException e) {
                GameClient.this.downService();
            }
        }
    }

    // нить отправляющая сообщения приходящие с консоли на сервер
    public class WriteMsg extends Thread {

        @Override
        public void run() {
            while (true) {
                try {
                    System.out.print("");
                    if (mainGameScreen.health < 0)
                        out.write("YouWin");
                } catch (IOException e) {
                    GameClient.this.downService();
                }
            }
        }
    }
}

