package ru.nsu.nosenko.towerdefense.model.tools;

import java.io.*;
import java.net.*;
import java.util.LinkedList;

class GameServer extends Thread {

    private Socket socket; // сокет, через который сервер общается с клиентом,
    private BufferedReader in; // поток чтения из сокета
    private BufferedWriter out; // поток записи в сокет

    public GameServer(Socket socket) throws IOException {
        this.socket = socket;
        // если потоку ввода/вывода приведут к генерированию исключения, оно проброситься дальше
        in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
        start();
    }
    @Override
    public void run() {
        String word;
        try {
            while (true) {
                word = in.readLine();
                for (GameServer vr : Server.serverList) {
                    if(word.equals("YouWin") )
                        vr.send("YouLose");
                }
            }
        } catch (NullPointerException | IOException ignored) {}
    }

    private void send(String msg) {
        try {
            out.write(msg);
            out.newLine();
            out.flush();
        } catch (IOException ignored) {}

    }
}
public class Server {
    public static final int PORT = 8080;
    public static LinkedList<GameServer> serverList = new LinkedList<>(); // список всех нитей - экземпляров
    // сервера, слушающих каждый своего клиента
    public static void main(String[] args) throws IOException {
        ServerSocket server = new ServerSocket(PORT);
        System.out.println("Server Started");
        try {
            while (true) {
                // Блокируется до возникновения нового соединения:
                Socket socket = server.accept();
                try {
                    serverList.add(new GameServer(socket)); // добавить новое соединенние в список
                    System.out.println("player connected");
                } catch (IOException e) {
                    socket.close();
                }
            }
        } finally {
            server.close();
        }
    }
}
