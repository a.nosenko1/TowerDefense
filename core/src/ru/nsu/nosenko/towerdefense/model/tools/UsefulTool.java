package ru.nsu.nosenko.towerdefense.model.tools;

import com.badlogic.gdx.graphics.g2d.Sprite;

public class UsefulTool {
    public static Sprite reduce(Sprite sprite, float scaleX, float scaleY){
        sprite.setSize(sprite.getWidth()*scaleX, sprite.getHeight()*scaleY);
        return sprite;
    }
}
