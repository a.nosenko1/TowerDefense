package ru.nsu.nosenko.towerdefense.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import ru.nsu.nosenko.towerdefense.StartGame;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MainMenuScreen implements Screen {
    SpriteBatch batch;          // для отрисовки
    OrthographicCamera camera;  // область просмотра

    Texture startButtonTexture; Sprite startButtonSprite;
    Texture loadButtonTexture;  Sprite loadButtonSprite;
    Texture mapButtonTexture;   Sprite mapButtonSprite;
    Texture exitButtonTexture;  Sprite exitButtonSprite;

    Texture authorTexture;          Sprite authorSprite;
    Texture nameTexture;            Sprite nameSprite;
    Texture backgroundBlackTexture; Sprite backgroundBlackSprite;
    Texture backgroundWhiteTexture; Sprite backgroundWhiteSprite;
    Texture soundTexture;           Sprite soundSprite;
    Texture soundOFFTexture;        Sprite soundOFFSprite;
    Texture ghostTexture;           Sprite ghostSprite;

    Sound sound;
    boolean soundIsPlaying = true;
    Sound buttonTouchedSound;

    ArrayList<Sprite> sprites;  // чтобы всем установить один размер в цикле

    private static float BUTTON_RESIZE_FACTOR = 2000f;  // размер кнопки (больше - меньше кнопка)
    private static float START_VERT_POSITION_FACTOR = 400f; // высота первой кнопки
    private static float DELTA_POSITION = 10f; // расстояние между кнопками
    private static float INCREASE_MUL = 0.4f; // во сколько раз увеличивается при наведении

    Map<Sprite, Vector2> normalSizes;       // обычные размеры, иниц в initSizes
    Map<Sprite, Vector2> increasedSizes;    // увеличенные размеры, иниц в initSizes

    private StartGame startGame;  // для изменения экрана игры

    public MainMenuScreen(SpriteBatch batch, StartGame startGame){

        this.startGame = startGame;

        float height    = Gdx.graphics.getHeight();
        float width     = Gdx.graphics.getWidth();

        camera = new OrthographicCamera(width, height);
        camera.setToOrtho(false);
        this.batch = batch;

        sound = Gdx.audio.newSound(Gdx.files.internal("mainMenu/Knightriders.mp3"));
        long soundID = sound.play();
        sound.setLooping(soundID, true);
        sound.setVolume(soundID, 0.4f);

        buttonTouchedSound = Gdx.audio.newSound(Gdx.files.internal("mainMenu/buttonTouched.mp3"));

        loadTextures();
        createSprites();
        initSizes(height, width);
        setSizes(normalSizes);
        backgroundBlackSprite.setSize(width, height);
        backgroundWhiteSprite.setSize(width, height);
        setPosition(height, width);
    }

    void loadTextures(){
        startButtonTexture = new Texture(Gdx.files.internal("mainMenu/start.png"));
        loadButtonTexture = new Texture(Gdx.files.internal("mainMenu/Load Game.png"));
        mapButtonTexture = new Texture(Gdx.files.internal("mainMenu/MapCreator.png"));
        exitButtonTexture = new Texture(Gdx.files.internal("mainMenu/Exit.png"));

        authorTexture = new Texture(Gdx.files.internal("mainMenu/by A. Nosenko.png"));
        nameTexture = new Texture(Gdx.files.internal("mainMenu/Cyber Tower Defense.png"));
        backgroundBlackTexture = new Texture(Gdx.files.internal("mainMenu/back.png"));
        backgroundWhiteTexture = new Texture(Gdx.files.internal("mainMenu/backwhite.png"));
        soundTexture = new Texture(Gdx.files.internal("mainMenu/sound.png"));
        soundOFFTexture = new Texture(Gdx.files.internal("mainMenu/soundOFF.png"));
        ghostTexture = new Texture(Gdx.files.internal("mainMenu/ghost.png"));
    }

    void createSprites(){
        startButtonSprite = new Sprite(startButtonTexture);
        loadButtonSprite = new Sprite(loadButtonTexture);
        mapButtonSprite = new Sprite(mapButtonTexture);
        exitButtonSprite = new Sprite(exitButtonTexture);

        authorSprite = new Sprite(authorTexture);
        nameSprite = new Sprite(nameTexture);
        backgroundBlackSprite = new Sprite(backgroundBlackTexture);
        backgroundWhiteSprite = new Sprite(backgroundWhiteTexture);
        soundSprite = new Sprite(soundTexture);
        soundOFFSprite = new Sprite(soundOFFTexture);
        ghostSprite = new Sprite(ghostTexture);

        sprites = new ArrayList<>();
        sprites.add(startButtonSprite);
        sprites.add(loadButtonSprite);
        sprites.add(mapButtonSprite);
        sprites.add(exitButtonSprite);
        sprites.add(authorSprite);
        sprites.add(nameSprite);
        sprites.add(soundSprite);
        sprites.add(soundOFFSprite);
        sprites.add(ghostSprite);

    }

    void initSizes(final float height, final float width){
        normalSizes = new HashMap<>();
        for (Sprite sprite : sprites) {
            normalSizes.put(sprite, new Vector2
                    (sprite.getWidth() * (width / BUTTON_RESIZE_FACTOR) / 2,
                     sprite.getHeight() * (height / BUTTON_RESIZE_FACTOR)));
        }

        increasedSizes = new HashMap<>();
        for (Sprite sprite : sprites) {
            increasedSizes.put(sprite, new Vector2(sprite.getWidth()*INCREASE_MUL,
                                                   sprite.getHeight()*INCREASE_MUL));
        }
    }

    void setSizes(Map<Sprite, Vector2> type){
        for (Sprite sprite : sprites) {
            sprite.setSize
                    (type.get(sprite).x,type.get(sprite).y);
        }
    }

    void setPosition(final float height, final float width){
        startButtonSprite.
                setPosition((width/3.2f-startButtonSprite.getWidth()/2),
                        START_VERT_POSITION_FACTOR);
        loadButtonSprite.
                setPosition((width/3.2f-loadButtonSprite.getWidth()/2),
                        START_VERT_POSITION_FACTOR-loadButtonSprite.getHeight()-DELTA_POSITION);
        mapButtonSprite.
                setPosition((width/3.2f-mapButtonSprite.getWidth()/2),
                        START_VERT_POSITION_FACTOR-mapButtonSprite.getHeight()-loadButtonSprite.getHeight()-2*DELTA_POSITION);
        exitButtonSprite.
                setPosition((width/3.2f-exitButtonSprite.getWidth()/2),
                        START_VERT_POSITION_FACTOR-exitButtonSprite.getHeight()-mapButtonSprite.getHeight()-loadButtonSprite.getHeight()-3*DELTA_POSITION);
        authorSprite.
                setPosition((width-authorSprite.getWidth()-10f),
                        0);
        nameSprite.setPosition((width/6f-startButtonSprite.getWidth()/2),
                START_VERT_POSITION_FACTOR+startButtonSprite.getHeight()+5*DELTA_POSITION);
        soundSprite.setPosition(10,10);
        soundOFFSprite.setPosition(10,10);
        ghostSprite.setPosition((3.7f*width/5f-ghostSprite.getWidth()/2),
                40);
    }

    void handlePointed(){   // по наведению
        Vector3 coord = new Vector3();
        coord.set(Gdx.input.getX(),Gdx.input.getY(),0);
        camera.unproject(coord);
        float touchX = coord.x;
        float touchY = coord.y;

        if (isThisCoordBelongToSprite(touchX, touchY, exitButtonSprite)){
            buttonTouchedSoundPlay(exitButtonSprite);
            setSizes(normalSizes);
            exitButtonSprite.setSize(increasedSizes.get(exitButtonSprite).x,
                                     increasedSizes.get(exitButtonSprite).y);
        } else if (isThisCoordBelongToSprite(touchX, touchY, startButtonSprite)) {
            buttonTouchedSoundPlay(startButtonSprite);
            setSizes(normalSizes);
            startButtonSprite.setSize(increasedSizes.get(startButtonSprite).x,
                    increasedSizes.get(startButtonSprite).y);
        } else if (isThisCoordBelongToSprite(touchX, touchY, mapButtonSprite)) {
            buttonTouchedSoundPlay(mapButtonSprite);
            setSizes(normalSizes);
            mapButtonSprite.setSize(increasedSizes.get(mapButtonSprite).x,
                    increasedSizes.get(mapButtonSprite).y);
        } else if (isThisCoordBelongToSprite(touchX, touchY, loadButtonSprite)) {
            buttonTouchedSoundPlay(loadButtonSprite);
            setSizes(normalSizes);
            loadButtonSprite.setSize(increasedSizes.get(loadButtonSprite).x,
                    increasedSizes.get(loadButtonSprite).y);
        } else if (isThisCoordBelongToSprite(touchX, touchY, soundSprite) && soundIsPlaying) {
            setSizes(normalSizes);
            soundSprite.setSize(increasedSizes.get(soundSprite).x,
                    increasedSizes.get(soundSprite).y);
        } else if (isThisCoordBelongToSprite(touchX, touchY, soundOFFSprite) && !soundIsPlaying) {
            setSizes(normalSizes);
            soundOFFSprite.setSize(increasedSizes.get(soundOFFSprite).x,
                    increasedSizes.get(soundOFFSprite).y);
        } else{
            lastPlayedSprite = null;
            setSizes(normalSizes);
        }
    }

    Sprite lastPlayedSprite = null;
    void buttonTouchedSoundPlay(Sprite sprite){
        if (lastPlayedSprite != sprite){
            sound.setVolume(buttonTouchedSound.play(), 0.4f);
        }

        lastPlayedSprite = sprite;
    }

    void handleTouch(){     // по нажатию
        Vector3 coord = new Vector3();
        if (Gdx.input.justTouched()){
            coord.set(Gdx.input.getX(),Gdx.input.getY(),0);
        }
        camera.unproject(coord);

        float touchX = coord.x;
        float touchY = coord.y;

        if (isThisCoordBelongToSprite(touchX, touchY, exitButtonSprite)){
            Gdx.app.exit();
        }
        if (isThisCoordBelongToSprite(touchX, touchY, startButtonSprite)){
            startGame.newMainGameScreen(1000);
            startGame.showMainGameScreen();
        }
        if (isThisCoordBelongToSprite(touchX, touchY, loadButtonSprite)){
            if (startGame.isMainGameExist())
                startGame.showMainGameScreen();
        }
        if (isThisCoordBelongToSprite(touchX, touchY, soundSprite) ||
            isThisCoordBelongToSprite(touchX, touchY, soundOFFSprite)){
            if (soundIsPlaying){
                sound.pause();
                soundIsPlaying = false;
            }
            else {
                sound.resume();
                soundIsPlaying = true;
            }
        }
    }

    boolean isThisCoordBelongToSprite(float x, float y, Sprite sprite){
        return (x >= sprite.getX()) &&
                (x <= sprite.getX() + sprite.getWidth()) &&
                (y >= sprite.getY()) &&
                (y <= sprite.getY() + sprite.getHeight());
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0f,0f,0f,1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        batch.setProjectionMatrix(camera.combined);

        batch.begin();

        startButtonSprite.draw(batch);
        loadButtonSprite.draw(batch);
        mapButtonSprite.draw(batch);
        exitButtonSprite.draw(batch);
        authorSprite.draw(batch);
        nameSprite.draw(batch);
        if (soundIsPlaying){
            soundSprite.draw(batch);
        } else
            soundOFFSprite.draw(batch);
        ghostSprite.draw(batch);
        ghostChangeCoord(delta);

        handlePointed();
        handleTouch();

        batch.end();
    }



    boolean isMovingUp = true;
    void ghostChangeCoord(float delta){
        if (isMovingUp){
            ghostSprite.setPosition(ghostSprite.getX(),
                    ghostSprite.getY() + 10f*delta);
            if (ghostSprite.getY() > 55)
                isMovingUp = false;
        }
        else{
            ghostSprite.setPosition(ghostSprite.getX(),
                    ghostSprite.getY() - 10f*delta);
            if (ghostSprite.getY() < 40)
                isMovingUp = true;
        }
    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        startButtonTexture.dispose();
        startButtonTexture.dispose();
        loadButtonTexture.dispose();
        mapButtonTexture.dispose();
        exitButtonTexture.dispose();
        authorTexture.dispose();
        nameTexture.dispose();
        backgroundBlackTexture.dispose();
        backgroundWhiteTexture.dispose();
        soundTexture.dispose();
        soundOFFTexture.dispose();
        buttonTouchedSound.dispose();
        ghostTexture.dispose();
        batch.dispose();
    }
}
