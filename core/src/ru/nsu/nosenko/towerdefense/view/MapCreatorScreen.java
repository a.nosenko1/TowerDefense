//package ru.nsu.nosenko.towerdefense.view;
//
//import com.badlogic.gdx.Gdx;
//import com.badlogic.gdx.Screen;
//import com.badlogic.gdx.graphics.GL20;
//import com.badlogic.gdx.graphics.OrthographicCamera;
//import com.badlogic.gdx.graphics.g2d.SpriteBatch;
//import com.badlogic.gdx.scenes.scene2d.*;
//import com.badlogic.gdx.utils.viewport.FitViewport;
//import ru.nsu.nosenko.towerdefense.StartGame;
//import ru.nsu.nosenko.towerdefense.model.cells.*;
//import ru.nsu.nosenko.towerdefense.model.map.Map;
//import ru.nsu.nosenko.towerdefense.model.turrets.FireTurret;
//
//
//public class MapCreatorScreen implements Screen {
//    private SpriteBatch batch;          // для отрисовки
//    private OrthographicCamera camera;  // область просмотра
//
//    // коэффициенты (0 -- 1) от текущей высоты и ширины окна
//    private static final float UP_AND_BOTTOM_FRAME_THICKNESS_PART = 0.05f;
//    private static final float MENU_PART = 0.3f;
//    private static final float FIELD_PART = 0.9f;
//
//    private Stage stage;
//    Map tempMap;
//    StartGame startGame;
//
//   // private enum BrushType {Barrier, Base, MobSpawner, Road, Turret, Nothing}
//  //  private BrushType currentBrushType = BrushType.Nothing;
//
//    public MapCreatorScreen(SpriteBatch batch, final StartGame startGame){
//        this.batch = batch;
//        this.startGame = startGame;
//
//        float height    = Gdx.graphics.getHeight();
//        float width     = Gdx.graphics.getWidth();
//
//        camera = new OrthographicCamera(width, height);
//        camera.setToOrtho(false);
//
//        stage = new Stage(new FitViewport(width, height, camera), batch);
//        Gdx.input.setInputProcessor(stage);
//
//        //tempMap = Map.MapFactory.newEmptyMap(16,12);
//        tempMap = Map.MapFactory.newSimpleMap();
//        stage.addActor(tempMap);
//
//        float SCALE_COEF_W = FIELD_PART*(1-MENU_PART)*width/ tempMap.getWidth();
//        float SCALE_COEF_H = (1-2*UP_AND_BOTTOM_FRAME_THICKNESS_PART)*height/ tempMap.getHeight();
//        tempMap.setScale(SCALE_COEF_W, SCALE_COEF_H);
//        tempMap.setPosition(width*(1-FIELD_PART)/2, UP_AND_BOTTOM_FRAME_THICKNESS_PART*height);
//
//        FireTurret fireTurret = new FireTurret((TurretCell)tempMap.field[5][3]);
//        ((TurretCell)tempMap.field[5][3]).setTurret(fireTurret);
//        tempMap.addActor(fireTurret);
//
//        //stage.addActor(fireTurret);
//        fireTurret.toFront();
//        fireTurret.levelUP();
//        fireTurret.levelUP();
//
////        Skin skin = new Skin(Gdx.files.internal("uiskin.json"));
////        Button cancel = new TextButton("Cancel", skin);
////        cancel.addListener(new ChangeListener() {
////            @Override
////            public void changed(ChangeEvent event, Actor actor) {
////                mainGame.showMenu();
////            }
////        });
////        stage.addActor(cancel);
////
////        Label label = new Label("234234 $", new Skin(Gdx.files.internal("label.json")));
////        stage.addActor(label);
////        label.setFontScale(0.7f);
//
//    }
//
//
//
//    @Override
//    public void show() {
//
//    }
//
//    @Override
//    public void render(float delta) {
//        Gdx.gl.glClearColor(0f,0f,0f,1);
//        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
//
//        batch.setProjectionMatrix(camera.combined);
//
//        stage.draw();
//        stage.act(Gdx.graphics.getDeltaTime());
//    }
//
//    @Override
//    public void resize(int width, int height) {
//
//    }
//
//    @Override
//    public void pause() {
//
//    }
//
//    @Override
//    public void resume() {
//
//    }
//
//    @Override
//    public void hide() {
//
//    }
//
//    @Override
//    public void dispose() {
//
//    }
//}
