package ru.nsu.nosenko.towerdefense.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import ru.nsu.nosenko.towerdefense.StartGame;
import ru.nsu.nosenko.towerdefense.model.buttons.MyButton;
import ru.nsu.nosenko.towerdefense.model.cells.TurretCell;
import ru.nsu.nosenko.towerdefense.model.listeners.BackChangeListener;
import ru.nsu.nosenko.towerdefense.model.listeners.ExitChangeListener;
import ru.nsu.nosenko.towerdefense.model.listeners.TurretCellListener;
import ru.nsu.nosenko.towerdefense.model.listeners.TurretMenuListener;
import ru.nsu.nosenko.towerdefense.model.map.Map;
import ru.nsu.nosenko.towerdefense.model.menu.TurretsMenu;
import ru.nsu.nosenko.towerdefense.model.mobs.Mob;
import ru.nsu.nosenko.towerdefense.model.mobs.MobFactory;
import ru.nsu.nosenko.towerdefense.model.tools.Server;
import ru.nsu.nosenko.towerdefense.model.turrets.*;

import java.util.ArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class MainGameScreen extends ScreenAdapter {
    private SpriteBatch batch;          // для отрисовки
    private OrthographicCamera camera;  // область просмотра
    final int[] i = {0};
    ScheduledExecutorService service;

    private Stage stage;
    private Map map;
    private StartGame startGame;


    Image field;

    Integer money;
    Label labelMoney;
    Integer wave;
    Label labelWave;
    Label healthLabel;
    public int health;


    public MainGameScreen(SpriteBatch batch, final StartGame startGame, int startMoney){
        Server server = new Server();

        this.batch = batch;
        this.startGame = startGame;
        this.money = startMoney;
        wave = 0;
        health = 5;

        float height    = Gdx.graphics.getHeight();
        float width     = Gdx.graphics.getWidth();

        camera = new OrthographicCamera(width, height);
        camera.setToOrtho(false);

        stage = new Stage(new StretchViewport(width, height, camera), batch);
        Gdx.input.setInputProcessor(stage);


        envCreate();

        map = Map.MapFactory.newSimpleMap();
        final TurretsMenu turretsMenu = new TurretsMenu(money, labelMoney, map);
        map.addActor(turretsMenu);
        turretsMenu.hide();

        final ArrayList<Actor> turretsCells = new ArrayList<>();

        for (int i = 0; i < map.getCellsX(); i++) {
            for (int j = 0; j < map.getCellsY(); j++) {
                if (map.field[i][j] != null){
                    if (!map.field[i][j].isVisible()){
                        map.field[i][j].addListener(new TurretCellListener(turretsMenu, map.field[i][j]));
                        map.field[i][j].setVisible(true);
                        turretsCells.add(map.field[i][j]);
                    }
                }
            }
        }
        stage.addActor(map);
        map.toFront();
        setFieldScale(map);

        turretsMenu.red1.addListener(new TurretMenuListener(turretsMenu, new FireTurret(new TurretCell(-1, -1), map)));
        turretsMenu.blue1.addListener(new TurretMenuListener(turretsMenu, new WaterTurret(new TurretCell(-1, -1), map)));
        turretsMenu.white1.addListener(new TurretMenuListener(turretsMenu, new IceTurret(new TurretCell(-1, -1), map)));
        turretsMenu.green1.addListener(new TurretMenuListener(turretsMenu, new GreenTurret(new TurretCell(-1, -1), map)));


    }
    public void addMoney(int money){
        this.money += money;
        labelMoney.setText("$ " + this.money);
    }
    private void startWave(int num){
        switch (num){
            case 2: {
                final Mob[] mobArray1 = new Mob[17];
                mobArray1[0] = MobFactory.newMob(MobFactory.MobType.YELLOW3, 1, 1425, 125, map,this); // это координаты центра (-25) клетки спавна
                mobArray1[1] = MobFactory.newMob(MobFactory.MobType.YELLOW2, 1, 1425, 125, map,this); // это координаты центра (-25) клетки спавна
                mobArray1[2] = MobFactory.newMob(MobFactory.MobType.BLUE2,   1, 1425, 125, map,  this); // это координаты центра (-25) клетки спавна
                mobArray1[3] = MobFactory.newMob(MobFactory.MobType.BLUE2,   1, 1425, 125, map,  this); // это координаты центра (-25) клетки спавна
                mobArray1[4] = MobFactory.newMob(MobFactory.MobType.BLUE2,   1, 1425, 125, map,  this); // это координаты центра (-25) клетки спавна
                mobArray1[5] = MobFactory.newMob(MobFactory.MobType.RED1, 2, 1425, 125, map,   this);//координаты центра (-25) клетки спавна
                mobArray1[6] = MobFactory.newMob(MobFactory.MobType.RED1, 2, 1425, 125, map,   this);//координаты центра (-25) клетки спавна
                mobArray1[7] = MobFactory.newMob(MobFactory.MobType.RED1, 2, 1425, 125, map,   this);//координаты центра (-25) клетки спавна
                mobArray1[8] = MobFactory.newMob(MobFactory.MobType.RED2, 2, 1425, 125, map,   this);//координаты центра (-25) клетки спавна
                mobArray1[9] = MobFactory.newMob(MobFactory.MobType.RED3, 2, 1425, 125, map,   this);//координаты центра (-25) клетки спавна
                mobArray1[10] = MobFactory.newMob(MobFactory.MobType.RED2, 2, 1425, 125, map,   this);//координаты центра (-25) клетки спавна
                mobArray1[11] = MobFactory.newMob(MobFactory.MobType.RED3, 2, 1425, 125, map,   this);//координаты центра (-25) клетки спавна
                mobArray1[12] = MobFactory.newMob(MobFactory.MobType.RED1, 2, 1425, 125, map,   this);//координаты центра (-25) клетки спавна
                mobArray1[13] = MobFactory.newMob(MobFactory.MobType.RED1, 2, 1425, 125, map,   this);//координаты центра (-25) клетки спавна
                mobArray1[14]= MobFactory.newMob(MobFactory.MobType.RED1, 2, 1425, 125, map,   this);//координаты центра (-25) клетки спавна
                mobArray1[15] = MobFactory.newMob(MobFactory.MobType.RED1, 2, 1425, 125, map,   this);//координаты центра (-25) клетки спавна
                mobArray1[16] = MobFactory.newMob(MobFactory.MobType.RED1, 2, 1425, 125, map,   this);//координаты центра (-25) клетки спавна

                Runnable runnable = new Runnable() {
                    public void run() {
                        map.addActor(mobArray1[i[0]++]);
                    }
                };

                service = Executors.newSingleThreadScheduledExecutor();
                service.scheduleAtFixedRate(runnable, 1000, 1500, TimeUnit.MILLISECONDS);
            return;
            }
            case 1: {
                final Mob[] mobArray2 = new Mob[17];
                mobArray2[0] = MobFactory.newMob(MobFactory.MobType.YELLOW3, 2, 1425, 125, map,this); // это координаты центра (-25) клетки спавна
                mobArray2[1] = MobFactory.newMob(MobFactory.MobType.YELLOW2, 2, 1425, 125, map,this); // это координаты центра (-25) клетки спавна
                mobArray2[2] = MobFactory.newMob(MobFactory.MobType.BLUE2, 2, 1425, 125, map,  this); // это координаты центра (-25) клетки спавна
                mobArray2[3] = MobFactory.newMob(MobFactory.MobType.BLUE2, 2, 1425, 125, map,  this); // это координаты центра (-25) клетки спавна
                mobArray2[4] = MobFactory.newMob(MobFactory.MobType.BLUE2, 2, 1425, 125, map,  this); // это координаты центра (-25) клетки спавна
                mobArray2[5] = MobFactory.newMob(MobFactory.MobType.RED1, 2, 1425, 125, map,   this);//координаты центра (-25) клетки спавна
                mobArray2[6] = MobFactory.newMob(MobFactory.MobType.RED1, 2, 1425, 125, map,   this);//координаты центра (-25) клетки спавна
                mobArray2[7] = MobFactory.newMob(MobFactory.MobType.RED1, 2, 1425, 125, map,   this);//координаты центра (-25) клетки спавна
                mobArray2[8] = MobFactory.newMob(MobFactory.MobType.RED2, 2, 1425, 125, map,   this);//координаты центра (-25) клетки спавна
                mobArray2[9] = MobFactory.newMob(MobFactory.MobType.RED3, 2, 1425, 125, map,   this);//координаты центра (-25) клетки спавна
                mobArray2[10] = MobFactory.newMob(MobFactory.MobType.RED2, 2, 1425, 125, map,   this);//координаты центра (-25) клетки спавна
                mobArray2[11] = MobFactory.newMob(MobFactory.MobType.RED3, 2, 1425, 125, map,   this);//координаты центра (-25) клетки спавна
                mobArray2[12] = MobFactory.newMob(MobFactory.MobType.RED1, 2, 1425, 125, map,   this);//координаты центра (-25) клетки спавна
                mobArray2[13] = MobFactory.newMob(MobFactory.MobType.RED1, 2, 1425, 125, map,   this);//координаты центра (-25) клетки спавна
                mobArray2[14]= MobFactory.newMob(MobFactory.MobType.RED1, 2, 1425, 125, map,   this);//координаты центра (-25) клетки спавна
                mobArray2[15] = MobFactory.newMob(MobFactory.MobType.RED1, 2, 1425, 125, map,   this);//координаты центра (-25) клетки спавна
                mobArray2[16] = MobFactory.newMob(MobFactory.MobType.RED1, 2, 1425, 125, map,   this);//координаты центра (-25) клетки спавна

                Runnable runnable = new Runnable() {
                    public void run() {
                        map.addActor(mobArray2[i[0]++]);
                    }
                };

                service = Executors.newSingleThreadScheduledExecutor();
                service.scheduleAtFixedRate(runnable, 1000, 1500, TimeUnit.MILLISECONDS);
            }
        }

    }

    public void reduceHealth(int health){
        this.health -= health;
        healthLabel.setText("lives " + this.health);
    }

    private void envCreate(){
        MyButton back = new MyButton(new Texture(Gdx.files.internal("mainGame/Back.png")));
        stage.addActor(back);
        back.setPosition(30, 2);
        back.addListener(new BackChangeListener(startGame));

        Image background = new Image(new Texture(Gdx.files.internal("mainGame/background.png")));
        stage.addActor(background);
        background.toBack();

        MyButton exit = new MyButton(new Texture(Gdx.files.internal("mainGame/Exit.png")));
        stage.addActor(exit);
        exit.setPosition(170, 22);
        exit.addListener(new ExitChangeListener());

        MyButton saveGame = new MyButton(new Texture(Gdx.files.internal("mainGame/Save game.png")));
        stage.addActor(saveGame);
        saveGame.setPosition(1040, 2);
        saveGame.addListener(new BackChangeListener(startGame));

        MyButton nextWave = new MyButton(new Texture(Gdx.files.internal("mainGame/Next  wave.png")));
        stage.addActor(nextWave);
        nextWave.setPosition(971, 452);

        nextWave.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                startWave(++wave);
                labelWave.setText("wave " + wave);
            }
        });

        field = new Image(new Texture(Gdx.files.internal("mainGame/field.png")));
        stage.addActor(field);
        field.setPosition(40, 119);

        Image wakeUp = new Image(new Texture(Gdx.files.internal("mainGame/Wake up.png")));
        stage.addActor(wakeUp);
        wakeUp.setPosition(200, 670);

        Image toDefense = new Image(new Texture(Gdx.files.internal("mainGame/to defense.png")));
        stage.addActor(toDefense);
        toDefense.setPosition(550, 645);

        labelMoney = new Label("$ " + money.toString(), new Skin(Gdx.files.internal("label.json")));
        stage.addActor(labelMoney);
        labelMoney.setPosition(980, 260);
        labelMoney.setFontScale(0.8f);

        healthLabel = new Label("lives " + health, new Skin(Gdx.files.internal("label.json")));
        stage.addActor(healthLabel);
        healthLabel.setPosition(980, 100);
        healthLabel.setFontScale(0.8f);

        labelWave = new Label("wave " + wave, new Skin(Gdx.files.internal("label.json")));
        stage.addActor(labelWave);
        labelWave.setPosition(980, 500);
        labelWave.setFontScale(0.8f);


        Image Keanu = new Image(new Texture(Gdx.files.internal("mainGame/Keanu.png")));
        stage.addActor(Keanu);
        Keanu.setPosition(60, 620);

    }

    private void setFieldScale(Actor gameObject){
        gameObject.setScale(0.947f*field.getWidth()/gameObject.getWidth(),
                0.9f*field.getHeight()/gameObject.getHeight());
        gameObject.setPosition(field.getX()+25.5f, field.getY()+27f);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0f,0f,0f,1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        if (i[0] == 16) {
            service.shutdown();
            i[0] = 0;
        }
        batch.setProjectionMatrix(camera.combined);

        stage.draw();
        stage.act(Gdx.graphics.getDeltaTime());

    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
        stage.getViewport().update(width, height);
    }
}
