package ru.nsu.nosenko.towerdefense.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import ru.nsu.nosenko.towerdefense.StartGame;


public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = "Tower Defense";
		config.width = 1280;
		config.height = 720;
		config.foregroundFPS = 30;
		new LwjglApplication(StartGame.getInstance(), config);
	}
}
